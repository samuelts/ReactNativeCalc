import React, { Component } from 'react';
import { StyleSheet, Dimensions, View } from 'react-native';
import Display from './display';
import CalcBtn from './calc-btn';

class Stack {
  constructor() {
    this.items = [];
  }

  push(elem) {
    this.items.push(elem);
  }

  pop() {
    if (this.items.length == 0) {
      return 'Underflow';
    }
    return this.items.pop();
  }

  peek() {
    return this.items[this.items.length - 1];
  }

  isEmpty() {
    const empty = this.items.length == 0 ? true : false;
    return empty;
  }

  printStack() {
    const str = '';
    this.items.map((elem) => str += " " + elem);
    return str;
  }
}

export default class Calculator extends Component {
  constructor(props) {
    super(props)

    this.state={
      result: '0'
    };

    this.appendVal = this.appendVal.bind(this);
    this.infixToPostfix = this.infixToPostfix.bind(this);
    this.postfixEval = this.postfixEval.bind(this);
    this.handleNeg = this.handleNeg.bind(this);
  }

  appendVal(value) {
    // If previous result is undefined or NaN start from 0
    let undef = false;
    if (this.state.result === 'undefined' || this.state.result === 'NaN') {
      undef = true;
    }
    const result = undef ? '0' : this.state.result;

    // Split result into last number, and everything before the last number (prefix)
    const breakPt = result.search(/(\(|;\(|[0-9.;]+)(?!.)/);
    const lastNum = result.substring(breakPt);
    const prefix = result.substring(0, breakPt);

    switch (value) {
      case 'Enter':
        if (result.length > 0) {
          this.infixToPostfix();
        }
        break;
      case 'Delete':
        this.setState({
          result: '0'
        });
        break;
      case ';':
        // Unary negation operator handling
        if (lastNum.charAt(0) === ';') {
          this.setState({
            result: prefix + lastNum.substring(1)
          });
        } else if (result === '0') {
          break;
        } else {
          this.setState({
            result: prefix + ';' + lastNum
          });
        }
        break;
      case '.':
        // If starting a number with decimal prefix 0
        if (/[()*/+-]/.test(result.charAt(result.length-1))) {
          value = '0.';
        } else if (result === '0') {
          value = '0.';
        }
        // If the last number contains a decimal don't add another (5.5.5 => 5.55)
        if (/\./.test(lastNum) && value == '.') {
          break;
        }
      default:
        let newRes = result;
        if (/[*/+-]/.test(value)) {
          // Prevent multiple sequential operators
          if (/[*/+-]/.test(result.charAt(result.length-1))) {
            newRes = result.substring(0,result.length-1);
          }
          // Prefix zero to standalone operators
          if (result === '0') {
            value = '0'+value;
          }
        }

        this.setState({
          result: result === '0' ? value : newRes + value
        });
        break;
    }
  }

  infixToPostfix() {
    var opstack = new Stack();
    var postfix = [];
    const input = this.state.result;
    const operator = /([()*/+-])/g;
    const nums = /\d+/;
    const prec = {
      '*': 3,
      '/': 3,
      '+': 2,
      '-': 2,
      '(': 1
    };

    var infix = input.split(operator).filter((elem) => elem != "");

    // First cleanup unmatched parenthesis
    let pCount = 0;
    for (let i = 0; i < infix.length; i++) {
      if (infix[i] === '(') {
        pCount++;
      } else if (infix[i] === ')') {
        if (pCount === 0) {
          infix.unshift('(');
          i++;
        } else {
          pCount--;
        }
      }
    }

    while (pCount > 0) {
      infix.push(')');
      pCount--;
    }

    // Then build postfix
    for (let i = 0; i < infix.length; i++) {
      const token = infix[i];
      // Inject explicit multiplication
      if (/[(0-9]/.test(infix[i+1])) {
        if (/[)0-9]/.test(token)) {
          infix.splice(i+1, 0, '*');
        }
      }
      if (nums.test(token)) {
        postfix.push(token);
      } else if (token === '(') {
        opstack.push(token);
      } else if (token === ')') {
        let topToken = opstack.pop();
        while (topToken !== '(') {
          postfix.push(topToken);
          topToken = opstack.pop();
        }
      } else if (token === ';') {
        opstack.push(infix[i+1]);
        opstack.push(token);
        i++;
      } else {
        while (!opstack.isEmpty() && prec[opstack.peek()] >= prec[token]) {
          postfix.push(opstack.pop());
        }
        opstack.push(token);
      }
    }

    while (!opstack.isEmpty()) {
      postfix.push(opstack.pop());
    }

    this.setState({
      result: postfix.join(' ')
    });

    // If postfix length is zero don't bother with evaluation
    if (postfix.length > 0) {
      this.postfixEval(postfix);
    }
  }

  postfixEval(postfix) {
    // Expected data type in stack is String
    var evalstack = new Stack();
    const operator = /[*/+-]/;
    const cf = 1e10;
    const doMath = {
      '+': (x,y) => ((x*cf) + (y*cf)) / cf,
      '-': (x,y) => ((x*cf) - (y*cf)) / cf,
      '*': (x,y) => ((x*cf) * (y*cf)) / (cf*cf),
      '/': (x,y) => (x*cf) / (y*cf),
    };

    for (let i = 0; i < postfix.length; i++) {
      if (operator.test(postfix[i])) {
        // If operator grab top two items from stack and operate on them
        const second = parseFloat(evalstack.pop());
        const first = parseFloat(evalstack.pop());
        const val = doMath[postfix[i]](first,second);
        evalstack.push(String(val));
      } else if (postfix[i] === ';') {
        const topdeck = evalstack.pop();
        if (topdeck.charAt(0) === '-') {
          evalstack.push(topdeck.substring(1));
        } else {
          evalstack.push('-'+topdeck);
        }
      } else {
        // Convert non-unary negative to unary for eval and push to stack
        evalstack.push(this.handleNeg(postfix[i], 'unary'));
      }
    }

    let finalVal = evalstack.peek();
    // Infinity is actually undefined
    if (finalVal === 'Infinity') {
      finalVal = 'undefined';
    }
    this.setState({
      result: this.handleNeg(finalVal, 'non-unary')
    });
  }

  handleNeg(str, action) {
    // Handle negation symbol (';')
    if (action === 'unary')
      if (str.charAt(0) === ';') {
        return str.replace(/;/g, '-');
      } else {
        return str;
      }
    else if (action === 'non-unary') {
      if (str.charAt(0) === '-') {
        return str.replace(/-/g, ';');
      } else {
        return str;
      }
    } else {
      console.log('handleNeg: Unrecognized action, returning input.');
      return str;
    }
  }

  render() {
    const numRow0 = [
      {keyCode: '(',
        keyVal: '(',
        id: 'left_paren'},
      {keyCode: ')',
        keyVal: ')',
        id: 'right_paren'},
      {keyCode: ';',
        keyVal: '+/-',
        id: 'posneg'},
      {keyCode: 'Delete',
        keyVal: 'AC',
        id: 'clear'}];

    const numRow1 = [
      {keyCode: '7',
        keyVal: '7',
        id: 'seven'},
      {keyCode: '8',
        keyVal: '8',
        id: 'eight'},
      {keyCode: '9',
        keyVal: '9',
        id: 'nine'},
      {keyCode: '/',
        keyVal: '/',
        id: 'Viewide'}];

    const numRow2 = [
      {keyCode: '4',
        keyVal: '4',
        id: 'four'},
      {keyCode: '5',
        keyVal: '5',
        id: 'five'},
      {keyCode: '6',
        keyVal: '6',
        id: 'six'},
      {keyCode: '*',
        keyVal: '*',
        id: 'multiply'}];

    const numRow3 = [
      {keyCode: '1',
        keyVal: '1',
        id: 'one'},
      {keyCode: '2',
        keyVal: '2',
        id: 'two'},
      {keyCode: '3',
        keyVal: '3',
        id: 'three'},
      {keyCode: '-',
        keyVal: '-',
        id: 'subtract'}];

    const numRow4 = [
      {keyCode: '0',
        keyVal: '0',
        id: 'zero'},
      {keyCode: '.',
        keyVal: '.',
        id: 'decimal'},
      {keyCode: 'Enter',
        keyVal: '=',
        id: 'equals'},
      {keyCode: '+',
        keyVal: '+',
        id: 'add'}];

    const calcRow0 = numRow0.map((key) => {
      return (
        <View
            key={key.keyCode}
            style={styles.btn}
        >
          <CalcBtn
            keyCode={key.keyCode}
            keyVal={key.keyVal}
            appendVal={this.appendVal}
          />
        </View>
      );
    });

    const calcRow1 = numRow1.map((key) => {
      return (
        <View
            key={key.keyCode}
            style={styles.btn}
        >
          <CalcBtn
            keyCode={key.keyCode}
            keyVal={key.keyVal}
            appendVal={this.appendVal}
          />
        </View>
      );
    });

    const calcRow2 = numRow2.map((key) => {
      return (
        <View
            key={key.keyCode}
            style={styles.btn}
        >
          <CalcBtn
            keyCode={key.keyCode}
            keyVal={key.keyVal}
            appendVal={this.appendVal}
          />
        </View>
      );
    });

    const calcRow3 = numRow3.map((key) => {
      return (
        <View
            key={key.keyCode}
            style={styles.btn}
        >
          <CalcBtn
            keyCode={key.keyCode}
            keyVal={key.keyVal}
            appendVal={this.appendVal}
          />
        </View>
      );
    });

    const calcRow4 = numRow4.map((key) => {
      return (
        <View
            key={key.keyCode}
            style={styles.btn}
        >
          <CalcBtn
            keyCode={key.keyCode}
            keyVal={key.keyVal}
            appendVal={this.appendVal}
          />
        </View>
      );
    });

    return (
      <View style={styles.wrap}>
        <View style={styles.displayWrap}>
          <Display disp={this.state.result.replace(/;/g, '-')} fontLoaded={this.props.fontLoaded} />
        </View>
        <View style={styles.btnWrap}>
          <View style={styles.row0}>
            {calcRow0}
          </View>
          <View style={styles.row1}>
            {calcRow1}
          </View>
          <View style={styles.row2}>
            {calcRow2}
          </View>
          <View style={styles.row3}>
            {calcRow3}
          </View>
          <View style={styles.row4}>
            {calcRow4}
          </View>
        </View>
      </View>
    );
  }
}

export const dim = {
  fullHeight: Dimensions.get('window').height,
  fullWidth: Dimensions.get('window').width
}

const styles = StyleSheet.create({
  wrap: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignContent: 'space-around',
    backgroundColor: '#504F53',
    marginTop: 50
  },
  displayWrap: {
    flex: 1
  },
  btnWrap: {
    flex: 3,
    flexDirection: 'column',
    marginTop: 10,
    marginBottom: 20
  },
  row0: {
    flex: 1,
    flexDirection: 'row',
  },
  row1: {
    flex: 1,
    flexDirection: 'row'
  },
  row2: {
    flex: 1,
    flexDirection: 'row'
  },
  row3: {
    flex: 1,
    flexDirection: 'row'
  },
  row4: {
    flex: 1,
    flexDirection: 'row'
  },
  btn: {
    flex: 1
  }
});