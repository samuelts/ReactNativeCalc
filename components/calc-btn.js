import React, { Component } from 'react';
import { Dimensions, StyleSheet, TouchableOpacity, Text, View, Vibration} from 'react-native';

export default class CalcBtn extends Component {
  constructor(props) {
    super(props);

    this.callCalc = this.callCalc.bind(this);
  }

  // Call callback function to prep audio for play
  callCalc() {
    Vibration.vibrate(5);
    this.props.appendVal(this.props.keyCode);
  }

  render() {
    return (
      <TouchableOpacity onPress={this.callCalc} style={styles.btn}>
        <View style={styles.view}>
          <Text style={styles.text}>
            {this.props.keyVal}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}

export const dim = {
  fullHeight: Dimensions.get('window').height,
  fullWidth: Dimensions.get('window').width
}

const styles = StyleSheet.create({
  view: {
    backgroundColor: '#F4E8DA',
    borderRadius: 10,
    margin: 10
  },
  text: {
    textAlign: 'center',
    textAlignVertical: 'center',
    color: '#333',
    lineHeight: dim.fullHeight/11,
    fontSize: 30,
    fontWeight: 'bold'
  }
});