import React, { Component }  from 'react';
import { Dimensions, Text } from 'react-native';

export const dim = {
  fullHeight: Dimensions.get('window').height,
  fullWidth: Dimensions.get('window').width
}

export default class Display extends Component {
  textStyle() {
    let currFont = this.props.fontLoaded ? 'spell-a' : 'monospace';
    return {
      fontSize: 50,
      fontFamily: currFont,
      padding: 10,
      textAlignVertical: 'center',
      textAlign: 'right',
      backgroundColor: '#021514',
      color: '#DFFEFD',
      textShadowColor: '#DFFEFD',
      textShadowOffset: {width: 0, height: 0},
      textShadowRadius: 20,
      borderRadius: 10,
      width: dim.fullWidth*0.9,
      height: dim.fullHeight/5
    }
  }

  render() {
    return (
      <Text style={this.textStyle()} selectable={true}>
      {this.props.disp}
      </Text>
    );
  }
}