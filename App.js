import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Font } from 'expo';

import Calculator from './components/calculator';

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      fontLoaded: false
    };
  }

  async componentDidMount() {
    await Font.loadAsync({
      'spell-a': require('./assets/fonts/spell-a.ttf'),
    });

    this.setState({fontLoaded: true});
  }

  render() {
    return (
      <View style={styles.container}>
        <Calculator fontLoaded={this.state.fontLoaded}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#504F53',
    alignItems: 'center',
    justifyContent: 'center',
  },
});